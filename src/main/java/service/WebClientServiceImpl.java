package service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import model.Email;
import model.User;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import util.HibernateUtil;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;

public class WebClientServiceImpl implements WebClientService {

	private static WebClientServiceImpl service;

	private WebClientServiceImpl() {

	}

	public static WebClientService getInstance() {
		if (service == null) {
			service = new WebClientServiceImpl();
		}
		return service;
	}

	public boolean addUser(User user) {

		boolean status = false;
		Session session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("inside service ");
		try {
			session.beginTransaction();
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			status = true;
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return status;
	}

	public String getUserMails(Credential credential) throws IOException {

		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		
		Gmail gmailService = new Gmail.Builder(transport, jsonFactory,
				credential).setApplicationName("Webclient").build();
		JSONObject ticketDetails = new JSONObject();
		ListMessagesResponse openMessages = gmailService.users().messages()
				.list("me")
				// .setLabelIds(labelIds)
				.setQ("is:all label:inbox").setMaxResults(new Long(10))
				.execute();
		ticketDetails.put("open", "" + openMessages.getResultSizeEstimate());

		ListMessagesResponse closedMessages = gmailService.users().messages()
				.list("me")
				// .setLabelIds(labelIds)
				.setQ("label:inbox label:closed").setMaxResults(new Long(1))
				.execute();
		ticketDetails
				.put("closed", "" + closedMessages.getResultSizeEstimate());

		ListMessagesResponse pendingMessages = gmailService.users().messages()
				.list("me")
				// .setLabelIds(labelIds)
				.setQ("label:inbox label:pending").setMaxResults(new Long(1))
				.execute();
		ticketDetails.put("pending",
				"" + pendingMessages.getResultSizeEstimate());

		ticketDetails.put("unassigned", "0");
		List<Message> messages = openMessages.getMessages();
		// List<Map> openTickets=new ArrayList<Map>();
		JSONArray openTickets = new JSONArray();
		String returnVal = "";
		// Print ID and snippet of each Thread.
		if (messages != null) {

			for (Message message : messages) {
				openTickets.put(new JSONObject(getBareGmailMessageDetails(
						message.getId(), gmailService)));
			}
			ticketDetails.put("openTicketDetails", openTickets);
		}
		return ticketDetails.toString();
	}

	private Map getBareGmailMessageDetails(String messageId, Gmail gmailService) {
		Map<String, Object> messageDetails = new HashMap<String, Object>();
		try {
			Message message = gmailService.users().messages()
					.get("me", messageId).setFormat("full")
					.setFields("id,payload,sizeEstimate,snippet,threadId")
					.execute();
			List<MessagePartHeader> headers = message.getPayload().getHeaders();
			for (MessagePartHeader header : headers) {
				if (header.getName().equals("From")
						|| header.getName().equals("Date")
						|| header.getName().equals("Subject")
						|| header.getName().equals("To")
						|| header.getName().equals("CC")) {
					messageDetails.put(header.getName().toLowerCase(),
							header.getValue());
				}
			}
			messageDetails.put("snippet", message.getSnippet());
			messageDetails.put("threadId", message.getThreadId());
			messageDetails.put("id", message.getId());
			// messageDetails.put("body",message.getPayload().getBody().getData());

		} catch (IOException ex) {
			Logger.getLogger(WebClientServiceImpl.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return messageDetails;

	}

	public GoogleTokenResponse getTokenResponse(String code,
			HttpTransport transport, JsonFactory jsonFactory)
			throws IOException {

		GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
				transport,
				jsonFactory,
				"983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com",
				"wkv9XUrEXcU_XI1zSPdtpH1z", code, "postmessage").execute();

		return tokenResponse;

	}

	public GoogleCredential getCredential(HttpTransport transport,
			JsonFactory jsonFactory, GoogleTokenResponse tokenResponse) {

		GoogleCredential credential = new GoogleCredential.Builder()
				.setJsonFactory(jsonFactory)
				.setTransport(transport)
				.setClientSecrets(
						"983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com",
						"wkv9XUrEXcU_XI1zSPdtpH1z").build()
				.setFromTokenResponse(tokenResponse);

		return credential;

	}

	
	public ArrayList<Email> getMails(JSONArray messageDetails ,User user){
		
		ArrayList<Email> emails = new ArrayList<Email>();
		
		for(int i=0;i<messageDetails.length();i++){
			JSONObject message = messageDetails.getJSONObject(i);
			String to = message.getString("to");
			String messageId = message.getString("id");
			String threadId = message.getString("threadId");
			String subject = message.getString("subject");
			String from = message.getString("from");
			String date = message.getString("date");
			String snippet = message.getString("snippet");
			
			if(to.contains(user.getEmailId())){
				to = user.getEmailId();
			}
			Email email = new Email();
			email.setTo(to);
			email.setFrom(from);
			/*email.setUser(user);*/
			email.setMessageId(messageId);
			email.setSubject(subject);
			email.setDate(date);
			email.setThreadId(threadId);
			
			emails.add(email);
		}
		 return emails;
	}
	
	public boolean saveMails(ArrayList<Email> emails,String emailId){
		
		boolean status = false;
		Session session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("inside service to save mails");
		try {
		
			session.beginTransaction();
			User user = (User)session.get(User.class, emailId);
			for(Email email:emails){
			user.addEmail(email);
			session.saveOrUpdate(email);
			}
			session.getTransaction().commit();
			status = true;
			
		} catch (HibernateException e) {
			if (session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}

		return status;
		
	}
	
	public  MimeMessage getMimeMessage(Credential credential, String userId, String messageId)
		      throws IOException, MessagingException {
		
		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		
		Gmail gmailService = new Gmail.Builder(transport, jsonFactory,
				credential).setApplicationName("Webclient").build();
		
		    Message message = gmailService.users().messages().get(userId, messageId).setFormat("raw").execute();

		    byte[] emailBytes = Base64.decodeBase64(message.getRaw());

		    Properties props = new Properties();
		    javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, null);

		    MimeMessage email = new MimeMessage(session, new ByteArrayInputStream(emailBytes));

		    return email;
		  }

	
	public String sendMail(String to,String from,String subject,String snippet,Credential credential) throws MessagingException, IOException{
		
		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();
		
		Gmail gmailService = new Gmail.Builder(transport, jsonFactory,
				credential).setApplicationName("Webclient").build();
		
		MimeMessage email = createMimeMessage(to, from, subject, snippet);
        Message message = createMessageWithEmail(email);
        message = gmailService.users().messages().send("me", message).execute();
 
        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
        if (message.getId() != null) {
            return "success";
        } else {
            return "fail";
        }
		
	}
	
	 private Message createMessageWithEmail(MimeMessage email)
	            throws MessagingException, IOException {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        email.writeTo(baos);
	        String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
	        Message message = new Message();
	        message.setRaw(encodedEmail);
	        return message;
	    }
	 
	 private MimeMessage createMimeMessage(String to, String from, String subject, String snippet) {
	        try {
	            Properties props = new Properties();
	            javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, null);
	            MimeMessage message = new MimeMessage(session);
	            // Set From: header field of the header.
	            message.setFrom(new InternetAddress(from));
	 
	            // Set To: header field of the header.
	            message.addRecipient(javax.mail.Message.RecipientType.TO,
	                    new InternetAddress(to));
	 
	            // Set Subject: header field
	            message.setSubject(subject);
	 
	            // Send the actual HTML message, as big as you like
	            message.setContent(snippet,
	                    "text/html");
	            return message;
	        } catch (MessagingException ex) {
	            Logger.getLogger(WebClientServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
	            return null;
	        }
	    }
}
