package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

import model.Email;
import model.User;

public interface WebClientService {

	public boolean addUser(User user);

	public String getUserMails(Credential credential) throws IOException;

	public GoogleTokenResponse getTokenResponse(String code,
			HttpTransport transport, JsonFactory jsonFactory)
			throws IOException;

	public GoogleCredential getCredential(HttpTransport transport,
			JsonFactory jsonFactory, GoogleTokenResponse tokenResponse);

	public ArrayList<Email> getMails(JSONArray messageDetails, User user);

	public boolean saveMails(ArrayList<Email> emails, String emailId);

	public MimeMessage getMimeMessage(Credential credential, String userId,
			String messageId) throws IOException, MessagingException;

	public String sendMail(String to, String from, String subject,
			String snippet, Credential credential) throws MessagingException,
			IOException;

}
