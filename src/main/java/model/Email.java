package model;

import javax.mail.Message;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "email")
public class Email {

	@Column(name = "to_email_id")
	private String to;

	@Column(name = "from_email_id")
	private String from;

	@Id
	@Column(name = "message_id")
	private String messageId;

	@Column(name = "subject")
	private String subject;
	
	
	/* private String snippet; */

	@Column(name = "thread_id")
	private String threadId;

	@Column(name = "date")
	private String date;

	/*@ManyToOne
	@JoinTable(name = "user_email_mapping", joinColumns = @JoinColumn(name = "to_email_id", referencedColumnName = "email_id"),inverseJoinColumns = @JoinColumn(name = "email_id", referencedColumnName = "to_email_id"))
	private User user;*/
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	/*
	 * public String getSnippet() { return snippet; } public void
	 * setSnippet(String snippet) { this.snippet = snippet; }
	 */
	public String getThreadId() {
		return threadId;
	}

	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	/*public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}*/

}
