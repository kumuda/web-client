package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Column(name = "name")
	private String name;

	@Id
	@Column(name = "email_id")
	private String emailId;
	
	@ManyToMany
	@JoinTable(name = "user_email_mapping", joinColumns = @JoinColumn(name = " email_id", referencedColumnName = "email_id"), inverseJoinColumns = @JoinColumn(name = "message_id", referencedColumnName = "message_id"))
	private List<Email> emails;
	

	public User(String name, String emailId) {
		this.name = name;
		this.emailId = emailId;
	}

	public User() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			User user = (User) object;
			if (this.name == user.getName()
					&& this.emailId == user.getEmailId()) {
				result = true;
			}
		}
		return result;
	}
	
	

	@Override
	public int hashCode() {
		return (int) this.name.hashCode() * this.emailId.hashCode();
	}
	
	@Override
	public String toString() {
		
		String user = "{ \"name\":" + "\"" + this.getName()
				+ "\"," + "\"emailId\":" + "\"" + this.getEmailId() + "\"}";
		return user;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(ArrayList<Email> emails) {
		this.emails = emails;
	}
	
	public void addEmail(Email email){
		this.emails.add(email);
	}
}
