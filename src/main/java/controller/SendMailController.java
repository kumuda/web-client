package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

import service.WebClientService;
import service.WebClientServiceImpl;

public class SendMailController extends HttpServlet {

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		WebClientService service = WebClientServiceImpl.getInstance();
		
		HttpSession session = request.getSession();
		GoogleCredential credential = (GoogleCredential) session
				.getAttribute("credential");
		
		String status = null;

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String email = buffer.toString();

		JSONObject jsonObject = new JSONObject(email);
		String to = (String)jsonObject.get("to");
		String subject = (String)jsonObject.get("subject");
		String snippet = (String)jsonObject.get("snippet");
		String from = (String)session.getAttribute("emailId");
		 try {
			status = service.sendMail(to, from, subject, snippet, credential);
		} catch (MessagingException e) {
			status = "fail";
			e.printStackTrace();
		}
		 
		 request.setCharacterEncoding("utf8");
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			out.println(status);
	}

}
