package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import service.WebClientService;
import service.WebClientServiceImpl;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

public class GetMailController extends HttpServlet {
	
	@Override
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		System.out.println("inside get mail controller");
		
		HttpSession session = request.getSession();
		GoogleCredential credential = (GoogleCredential) session.getAttribute("credential");
		String emailId = (String) session.getAttribute("emailId");
		
		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String userInfo = buffer.toString();

		JSONObject jsonObject = new JSONObject(userInfo);
		String messageId = (String) jsonObject.get("messageId");
		
		WebClientService service = WebClientServiceImpl.getInstance();
		
		try {
			MimeMessage mimeMessage = service.getMimeMessage(credential, emailId, messageId);
			if(mimeMessage.getContent().toString().contains("MimeMultipart")){
				MimeMultipart multipart = (MimeMultipart) mimeMessage.getContent();
				 StringBuilder newBuffer = new StringBuilder();
				for (int i = 0; i < multipart.getCount(); i++) {  
				     BodyPart bodyPart = multipart.getBodyPart(i);  
				     InputStream stream = bodyPart.getInputStream(); 
				     BufferedReader br = new BufferedReader(new InputStreamReader(stream));  
				  
				      while (br.ready()) {  
				    	  newBuffer.append(br.readLine());  
				      }  
				    
				    }
				
				
				
				request.setCharacterEncoding("utf8");
				response.setContentType("application/octet-stream");
				PrintWriter out = response.getWriter();
				out.println(newBuffer);

				
			}
			
			else{
				
				request.setCharacterEncoding("utf8");
				response.setContentType("application/json");
				PrintWriter out = response.getWriter();
				out.println(mimeMessage.getContent().toString());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
