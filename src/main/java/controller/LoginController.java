package controller;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;

import org.json.JSONObject;

import service.WebClientService;
import service.WebClientServiceImpl;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Tokeninfo;
import com.google.gson.Gson;

public class LoginController extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		WebClientService service = WebClientServiceImpl.getInstance();

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		String userInfo = buffer.toString();

		JSONObject jsonObject = new JSONObject(userInfo);
		String code = (String) jsonObject.get("code");
		String emailId = (String) jsonObject.get("emailId");
		String name = (String) jsonObject.get("name");

		User user = new User(name, emailId);
		service.addUser(user);

		HttpTransport transport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		GoogleTokenResponse tokenResponse = service.getTokenResponse(code,
				transport, jsonFactory);

		GoogleCredential credential = service.getCredential(transport,
				jsonFactory, tokenResponse);

		Oauth2 oauth2 = new Oauth2.Builder(transport, jsonFactory, credential)
				.build();
		Tokeninfo tokenInfo = oauth2.tokeninfo()
				.setAccessToken(credential.getAccessToken()).execute();
		// If there was an error in the token info, abort.
		if (tokenInfo.containsKey("error")) {
			System.out.println("error");
		}

		if (!tokenInfo
				.getIssuedTo()
				.equals("983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com")) {
			response.setStatus(401);
			String message = "Token's client ID does not match app's.";
			Gson gson = new Gson();
			request.setAttribute("message", gson.toJson(message));
			System.out.println(message);

		}
		Gson gson = new Gson();
		String message = "successfully connected to user.";
		request.setAttribute("message", gson.toJson(message));

		/*
		 * response.setHeader("Access-Control-Allow-Origin", origin);
		 * response.setHeader("Access-Control-Allow-Methods", VALID_METHODS);
		 * 
		 * String headers = request.getHeader("Access-Control-Request-Headers");
		 * if (headers != null)
		 * response.setHeader("Access-Control-Allow-Headers", headers);
		 * 
		 * // Allow caching cross-domain permission
		 * response.setHeader("Access-Control-Max-Age", "3600");
		 */
		System.out.println("successfully connected to user");

		HttpSession session = request.getSession();
		session.setAttribute("code", code);
		session.setAttribute("emailId", emailId);
		session.setAttribute("name", name);
		session.setAttribute("tokenResponse", tokenResponse);
		session.setAttribute("credential", credential);
	}

}
