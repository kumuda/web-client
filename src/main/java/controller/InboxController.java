package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Email;
import model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import service.WebClientService;
import service.WebClientServiceImpl;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;

public class InboxController extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		WebClientService service = WebClientServiceImpl.getInstance();
		HttpSession session = request.getSession();
		GoogleCredential credential = (GoogleCredential) session
				.getAttribute("credential");
		String name = (String) session.getAttribute("name");
		String emailId = (String) session.getAttribute("emailId");

		User user = new User(name, emailId);

		String inbox = service.getUserMails(credential);

		JSONObject messageDetails = new JSONObject(inbox);

		JSONArray openTicketDetails = messageDetails
				.getJSONArray("openTicketDetails");
		ArrayList<Email> emails = service.getMails(openTicketDetails, user);
		service.saveMails(emails, emailId);
		request.setAttribute("inbox", inbox);
		request.setCharacterEncoding("utf8");
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.println(inbox);

	}
}
