function signInCallback(authResult) { 	 

	console.log(authResult);

	var emailId;
	var name;

	gapi.client.load('plus', 'v1', function() {
		gapi.client.plus.people.get({
			'userId' : 'me'
		}).execute(function(resp) {
			// Shows profile information
			resp.emails.forEach(function(email) {
				emailId = email.value;
			});
			name = resp.displayName;
			console.log(resp);

			if (authResult['code']) {
				//$('#signinButton').attr('style', 'display: none');
				$('#myModal').modal('hide');
				var data = {
					'code' : authResult.code,
					'emailId' : emailId,
					'name' : name
				};
				// var json = JSON2.stringify(data);
				$.ajax({
					type : 'POST',
					url : '/Webclient/loginController',
					contentType : "application/json; charset=utf-8",
					data : JSON.stringify(data),
					success : function(result) {
						
						/*var response = httpGet("http://localhost:8080/Webclient/sample");
						xmlDoc = $.parseXML( response ),
						  $xml = $( xmlDoc ),
						  $title = $xml.find( "title" );
							var a = $xml.find( "title" );
							console.log(a);*/
						
						$.get("/Webclient/inboxController", function(data){
							 var inbox = data.openTicketDetails;
							 inboxDisplay(inbox);
						});
					},
					processData : false,

				});
			} else if (authResult['error']) {
				// There was an error.
				// Possible error codes:
				//   "access_denied" - User denied access to your app
				//   "immediate_failed" - Could not automatially log in the user
				console.log('There was an error: ' + authResult['error']);
			}

		})

	});

}

function inboxDisplay(data){
	
	var table = $("#table");
	 var row;
	 $("#table tbody").remove();
	 data.forEach(function(email){
		 row = $("<tr></tr>");
		 	$("<td />").addClass("email-select").append($("<input />").attr({type:"checkbox",class:"checkbox",value:email.id})).appendTo(row);
			$("<td>"+email.from+"</td>").appendTo(row);
			$("<td>"+email.subject+"</td>").appendTo(row);
			$("<td>"+email.date+"</td>").appendTo(row);
			row.click(viewMail());
			row.appendTo(table);
	 });
}


	
$(document).ready(function(){


	$(window).load(function(){
		$('#myModal').modal('show');
	    });


	$('#compose').click(function(){
		$('#composeModal').modal('show');
	});

	
	$('#send_mail').click(function(){
		
		var to = $('#to_email_ids').val();
		var subject = $('#subject').val();
		var snippet = $('#snippet').val();
		
		var data = {'to':to,
				    'subject':subject,
				    'snippet':snippet
				    };
		
		if(to.length != 0){
		$.ajax({
			  type: "POST",
			  url: "/Webclient/sendMail",
			  contentType : "application/json; charset=utf-8",
			  data: JSON.stringify(data),
			  success: function(result){
				  
				  if(result=="success"){
				  alert('Message sent.');
				  }
				  else
					  {
					  alert('Sending failed.');
					  }
				  
				  
			  }
			  
			});
		
		}
		else{
			alert('to field left blank.');
		}
		
		$('#composeModal').modal('toggle');
	});
	

	});

function viewMail(){

$('#table tbody').unbind().on('click','tr td:not(.email-select)',function () {
	
	var messageId = $(this).parent().children('td:eq(0)').children().val();
	var from = $(this).parent().children('td:eq(1)').text();
	var subject = $(this).parent().children('td:eq(2)').text();
	var dateAndTime = $(this).parent().children('td:eq(3)').text();
	/*$.post("/Webclient/getMail", {messageId:messageId}, function(data){*/
	var data = {
			'messageId' : messageId
		};
	$.ajax({
		  type: "POST",
		  url: "/Webclient/getMail",
		  data: JSON.stringify(data),
		  success: function(result){
			  $('#email_subject').text(subject);
			  $('#sender_name').text(from);
			  $('#date_and_time').text(dateAndTime);
			  $('#messageBody').empty();
			  $('#messageBody').append(result);
			  $('#emailModal').modal('show');		
		  }
		  
		});
		
		
	});

  
	
	
}