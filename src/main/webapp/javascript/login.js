
function signInCallback(authResult) {
	  
	  console.log(authResult);
	  
	  var emailId;	
	  var name;

		gapi.client.load('plus', 'v1', function() {
		  gapi.client.plus.people.get( {'userId' : 'me'} ).execute(function(resp) {
		    // Shows profile information
			 resp.emails.forEach(function(email){
				emailId = email.value; 
			 });
			  name = resp.displayName;
		    console.log(resp);
		    
		    
		    if (authResult['code']) {
				  $('#signinButton').attr('style', 'display: none');
				  
				  var data = {'code':authResult.code,'emailId':emailId,'name':name};
				 // var json = JSON2.stringify(data);
				  $.ajax({
				      type: 'POST',
				      url: '/Webclient/loginController',
				      contentType:"application/json; charset=utf-8",
				      data: JSON.stringify(data),
				      success: function(result) {
				        // Handle or verify the server response if necessary.

				        // Prints the list of people that the user has allowed the app to know
				        // to the console.
				        console.log(result);
				        /*if (result['profile'] && result['people']){
				          $('#results').html('Hello ' + result['profile']['displayName'] + '. You successfully made a server side call to people.get and people.list');
				        } else {
				          $('#results').html('Failed to make a server-side call. Check your configuration and console.');
				        }*/
				        
				        /*window.location.assign("/Webclient/inbox");*/
				    	  
				      },
				      processData: false,
				     
				    });
				  } else if (authResult['error']) {
				    // There was an error.
				    // Possible error codes:
				    //   "access_denied" - User denied access to your app
				    //   "immediate_failed" - Could not automatially log in the user
				      console.log('There was an error: ' + authResult['error']);
				  }
				  
		  })
		  
		  
		});
	  
	}

	