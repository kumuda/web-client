<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/bootstrap.js"></script>
<script type="text/javascript" src="javascript/home.js"></script>
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script type="text/javascript" src="javascript/xml2json.js"></script>
<script type="text/javascript" src="javascript/xml2json.min.js"></script>
<script type="text/javascript" src="javascript/json2.js"></script>	


<title>Webclient</title>
</head>
<body>
	<div class="preview__header">
		<div class="form-group row">
			<div class="col-md-2 ">
				<div class="preview__envato-logo">
					<h4>WebClient</h4>
				</div>
			</div>

			<div class="container">
				<ul class="search-nav navbar-nav">
					<div class="search-row">
						<div class="col-mod-8">

							<input type="text" name="Search" placeholder="search" id="search"
								class="form-control focusedInput" />
						</div>



					</div>
				</ul>
				<button type="button" name="Delete" id="delete"
					class=" search-btn search-btn-primary">
					<span class="glyphicon glyphicon-search"> </span>
				</button>
			</div>

		</div>
	</div>
	<div class="body-container">
		<div class="menu">
			<div class="compose-container">
				<input type="button" class="btn btn-compose" name="compose" id='compose'
					value="Compose" />
			</div>
			<div class="compose-container">
				<a href="inbox.html">Inbox</a>
				<div class="spacer10"></div>
				<a href="inbox.html">Sent</a>
				<div class="spacer10"></div>
				<a href="inbox.html">Drafts</a>
				<div class="spacer10"></div>
				<a href="inbox.html">Trash</a>
			</div>
		</div>


		<div class="table-container">
			<div align="left" width="100%">
				<button type="button" name="Delete" id="delete"
					class=" search-btn search-btn-primary">
					<span class="glyphicon glyphicon-trash"> </span>
				</button>
			</div>
			<div class="spacer10"></div>
			<table class="table" id="table" border="0">
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
	
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true" disabled>&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Sign-in with Gmail.</h4>
				</div>
				<div class="modal-body">
					<div style="position: relative;">
						<span id="signinButton"> 
						  <span class="g-signin"
				                data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://mail.google.com/mail/feed/atom "
				                data-clientid="983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com"
				                data-redirecturi="postmessage" data-accesstype="offline"
				                data-cookiepolicy="single_host_origin"
				                data-callback="signInCallback"> 
			              </span>
			            </span>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	
	 <div class="modal fade" id="composeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
              <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Message</h4>
               </div>
               <div class="modal-body">
               		<label>To</label><input type="text" class="form-control focusedInput" id="to_email_ids" name="to"/>
               		<label>Subject</label><input type="text" class="form-control focusedInput" id="subject" name="subject"/>
               		<div class="spacer30"></div>
                    <textarea class="form-control" id="snippet" style="height:200px"></textarea>
               </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="send_mail">Send</button>
                    <button type="button" class="btn btn-default" name="discard draft" id="discard_draft" data-dismiss="modal">
					<span class="glyphicon glyphicon-trash"> </span>
				</button>
                </div>
             </div>
         </div>
     </div>
     
     
     
     <div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
              <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="email_subject"></h4>
               </div>
               <div class="modal-body" id="email_body">
                   <div class="container">
                      <div class="left-container">
                           <img src="lib/images/profile_mask2.png" alt="user" style="width:33px;height:33px" float="left">
                      </div>
                   <div class="right-container" id="div_sender_name">
                          <h5 id="sender_name"><strong ></strong></h5> 
                   </div>
                   <div class="right-container" id="div_sender_email_id">
                        <h5 id="sender_email_id"></h5>
                   </div>
                 <div class="right-container" id="div_date_and_time">
                     
                         <h5 id="date_and_time"></h5>
                      
                </div>
               		<div class="container" id="messageBody">
               		
               		
               		</div>
                    
               </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Reply</button>
                    <button type="button" class="btn btn-primary">Forward</button>
                    <button type="button" class="btn btn-default" name="delete email" id="delete_email" data-dismiss="modal">
					<span class="glyphicon glyphicon-trash"> </span>
				</button>
                </div>
             </div>
         </div>
     </div>
  </div>      

	<script type="text/javascript" src="js/htmlbox.content.js"></script>
</body>
</html>