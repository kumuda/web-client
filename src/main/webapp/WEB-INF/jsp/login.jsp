<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>WebClient</title>
<script src="https://apis.google.com/js/client:platform.js" async defer></script>
<script type="text/javascript" src="javascript/login.js"></script>
<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/json2.js"></script>
</head>
<body>
	<div class="modal hide fade">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4>Sign in with Gmail Account</h4>
       </div>
       
       <div class="modal-body">
			<span id="signinButton"> <!-- <span
    class="g-signin"
    data-callback="signinCallback"
    data-clientid="983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com"
    data-cookiepolicy="single_host_origin"
    data-requestvisibleactions="http://schema.org/AddAction"
    data-scope="https://www.googleapis.com/auth/plus.login">
  </span> --> <span class="g-signin"
				data-scope="https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://mail.google.com/mail/feed/atom"
				data-clientid="983294006428-f1knshk8gd9lf1s26i2u8fefubisfia3.apps.googleusercontent.com"
				data-redirecturi="postmessage" data-accesstype="offline"
				data-cookiepolicy="single_host_origin"
				data-callback="signInCallback"> 
			  </span>
			</span>
			</div>
	</div>
</body>
</html>