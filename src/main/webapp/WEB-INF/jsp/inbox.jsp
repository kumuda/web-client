<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">

<script type="text/javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" src="javascript/json2.js"></script>

<title>Insert title here</title>
</head>
<body>
<div class="preview__header">
    <div class="form-group row">
		<div class="col-md-2 ">
		   <div class="preview__envato-logo">
			<h4>WebClient</h4>
		  </div>
		</div>
	       
		   <div class="container">
			<ul class="search-nav navbar-nav">
                             <div class="search-row">
                                   <div class="col-mod-8" >
				
					<input type="text" 
				               name="Search" placeholder="search" id="search"
				               class="form-control focusedInput" />
				    </div>
				
					
				
			     </div>
			</ul>
			<button type="button" name="Delete" id="delete" class=" search-btn search-btn-primary">
		            <span class="glyphicon glyphicon-search">
                            </span>
			</button>
	         </div>
	    
     </div>
</div>
<div class="body-container">
<div class="menu">
<div class="compose-container">
<input type="button" class="btn btn-compose" name="compose" value="Compose" />
</div>
<div class="compose-container">
<a href="inbox.html">Inbox</a>
<div class="spacer10"></div>
<a href="inbox.html">Sent</a>
<div class="spacer10"></div>
<a href="inbox.html">Drafts</a>
<div class="spacer10"></div>
<a href="inbox.html">Trash</a>
</div>
</div>


<div class="table-container">
<div align="left" width="100%">
<button type="button" name="Delete" id="delete" class=" search-btn search-btn-primary">
		            <span class="glyphicon glyphicon-trash">
                            </span>
			</button>
</div>
<div class="spacer10"></div>
<table class="table" id="table" border="0">
<tbody>
</tbody>		
</table>
</div>
</div>
</body>
</html>